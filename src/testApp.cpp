#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
    shader.load("app.vert", "app.frag");
    sphere.setRadius(200.0f);
    sphere.setResolution(48);
    
    ofMesh mesh = sphere.getMesh();
    
    displacements = new float[mesh.getNumVertices()];
    if (displacements == 0) {
        printf("error allocating memory");
    }
    for (int i = 0; i < mesh.getNumVertices(); ++i) {
        displacements[i] = ofRandom(50);
    }
}

//--------------------------------------------------------------
void testApp::update(){
}

//--------------------------------------------------------------
void testApp::draw(){
    glEnable(GL_DEPTH_TEST);
    shader.begin();
    shader.setUniform1f("time", ofGetElapsedTimef());
    shader.setAttribute1fv("displacement", displacements);
    ofTranslate(ofGetWidth() * 0.5f, ofGetHeight() * 0.5f);
    ofRotateY(sin(ofGetElapsedTimef()) * 180);
    sphere.draw();
    shader.end();
    glDisable(GL_DEPTH_TEST);
}

void testApp::exit() {
    delete[] displacements;
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}