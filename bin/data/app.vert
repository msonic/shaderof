uniform float time;
varying vec3 normal;
attribute float displacement;

void main()
{
    normal = normalize(gl_NormalMatrix * gl_Normal);
    vec3 position = vec3(gl_Vertex[0], gl_Vertex[1], gl_Vertex[2]);
    vec3 newPosition = position + normal * cos(time) * displacement;
    gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * vec4(newPosition,1.0);
}
