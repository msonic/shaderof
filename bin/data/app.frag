uniform float time;
varying vec3 normal;

void main() {
    vec3 light = vec3(cos(time) * 50.0, cos(time) * 20.0, 10);
    light = normalize(light);
    float dProd = max(0.0, dot(normal, light));
    
    gl_FragColor = vec4(dProd * cos(time), dProd * sin(time), dProd, 1.0);
}